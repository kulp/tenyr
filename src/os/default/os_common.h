#ifndef OS_COMMON_H_
#define OS_COMMON_H_

#include <stddef.h>

typedef size_t lfind_size_t;

#include "os_base.h"

#endif

/* vi: set ts=4 sw=4 et: */
